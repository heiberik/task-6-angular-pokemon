import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'


import { AppComponent } from './app.component';
import { LandingComponent } from './pages/landing/landing.component';
import { TrainerComponent } from './pages/trainer/trainer.component';
import { CatalogueComponent } from './pages/catalogue/catalogue.component';
import { DetailComponent } from './pages/detail/detail.component';
import { AuthGuard } from './guards/auth.guard';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { NavbarComponent } from './components/navbar/navbar.component'

const routes: Routes = [
    { path: "landing", component: LandingComponent },
    { path: "trainer", component: TrainerComponent, canActivate: [AuthGuard] },
    { path: "catalogue", component: CatalogueComponent, canActivate: [AuthGuard] },
    { path: "detail", component: DetailComponent, canActivate: [AuthGuard] },
    { path: "", pathMatch: "prefix", redirectTo: "/catalogue" }
]


@NgModule({
    declarations: [
        AppComponent,
        LandingComponent,
        TrainerComponent,
        CatalogueComponent,
        DetailComponent,
        PokemonComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        HttpClientModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
