import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service'
import { SessionService } from '../../services/session/session.service';

@Component({
    selector: 'app-catalogue',
    templateUrl: './catalogue.component.html',
    styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit {

    pokemonList: any[] = []
    pokemonListShow: any[] = []
    pokemonListFiltered: any[] = []

    filterText: string = ""
    isLoading: boolean = false
    hasLoadedImgs: boolean = false

    constructor(private api: ApiService, private session: SessionService) {
        
    }

    ngOnInit(): void {
        const savedTrainer = this.session.get()

        if (savedTrainer.pokemon.length === 0) {

            this.api.getAllPokemon()
                .then((res) => {
                    this.pokemonList = res.results.filter(pokemon => {
                        if (pokemon.name.includes("-")) return false
                        else return true
                    })

                    this.session.save({
                        name: savedTrainer.name,
                        pokemon: this.pokemonList,
                        collectedPokemons: []
                    })
                    this.loadPokemonImages()
                })
                .catch(e => console.log(e))
        }
        else {
            this.pokemonList = this.session.get().pokemon
            this.loadPokemonImages()
        }
    }


    filterChangeHandler(text){
        this.pokemonListFiltered = this.pokemonListShow.filter(p => {return p.name.includes(text)})
    }

    loadPokemonImages(){

        this.pokemonList.forEach(pokemon => {
            this.api.getPokemonByName(pokemon.name)
                .then(res => {
                    pokemon.img = res.sprites.front_default
                    this.pokemonListShow.push(pokemon)
                    if (this.pokemonListShow.length === this.pokemonList.length){
                        this.isLoading = false
                        this.pokemonListFiltered = this.pokemonListShow
                    }
                })
                .catch(e => console.log(e))    
        })
    }
}



