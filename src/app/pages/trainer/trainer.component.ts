import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  pokemonListNames: any[] = []
  pokemonListShow: any[] = []
  isLoading: boolean = true

  constructor(private session: SessionService, private api: ApiService) { }

  ngOnInit(): void {
    const saved = this.session.get()

    this.pokemonListNames = saved.collectedPokemons

    this.pokemonListNames.forEach(name => {
      this.api.getPokemonByName(name)
        .then(res => {

          const pokemon = {
            img: res.sprites.front_default,
            name: name
          }
          
          this.pokemonListShow.push(pokemon)
          if (this.pokemonListShow.length === this.pokemonListNames.length) {
            this.isLoading = false
          }
        })
        .catch(e => console.log(e))
    })
  }

}
