import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session/session.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.css']
})

export class LandingComponent implements OnInit {


    trainerName: string = ""
 
    constructor(
        private session: SessionService,
        private router: Router) {

        if (this.session.get() !== false){
            this.router.navigateByUrl("/catalogue")
        }
    }

    ngOnInit(): void {

    }

    onNameChosenClicked() {
        this.session.save({
            name: this.trainerName,
            pokemon: [],
            collectedPokemons: []
        })
        this.router.navigateByUrl("/catalogue")
    }

}
