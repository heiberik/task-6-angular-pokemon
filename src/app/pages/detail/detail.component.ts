import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api/api.service'
import { SessionService } from 'src/app/services/session/session.service';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})

export class DetailComponent implements OnInit {

    name: string
    private sub: any
    loading: boolean = true
    pokemon: any = null
    catched: boolean = false

    constructor(private route: ActivatedRoute, private api: ApiService, private session: SessionService) { }

    ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {
            this.name = params['name']

            this.api.getPokemonByName(this.name)
                .then(res => {
                    this.loading = false
                    this.pokemon = res

                    const saved = this.session.get()

                    if (saved.collectedPokemons.includes(this.pokemon.name)) {
                        this.catched = true
                    }
                })
        })
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    handleCatchPokemonClick() {
        const saved = this.session.get()

        if (!this.catched) {
            saved.collectedPokemons.push(this.pokemon.name)
            this.session.save(saved)
        } else {
            saved.collectedPokemons = saved.collectedPokemons.filter(name => {
                if (name === this.pokemon.name) return false
                else return true
            })
            this.session.save(saved)
        }
        this.catched = !this.catched
    }
}
