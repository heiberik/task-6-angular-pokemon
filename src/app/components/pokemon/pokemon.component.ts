import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {

    @Input() pokemon: any
    loading: boolean = true

    constructor(private router: Router) { }

    ngOnInit(): void {

    }

    imageLoadedHandler() {
        this.loading = false
    }

    cardClickedHandler() {
        this.router.navigate(["/detail", { name: this.pokemon.name }])
    }

}
