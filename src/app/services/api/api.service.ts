import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment'

@Injectable({
    providedIn: 'root'
})

export class ApiService {

    constructor(private http: HttpClient) { }

    getAllPokemon(): Promise<any> {
        return this.http.get(`${environment.apiUrl}v2/pokemon?limit=151&offset=0`).toPromise()
    }

    getPokemonByName(name: string): Promise<any> {
        return this.http.get(`${environment.apiUrl}v2/pokemon/${name}`).toPromise()
    }
}
