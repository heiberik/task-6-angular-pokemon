import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class SessionService {

    constructor() { }

    save(session: any){
        localStorage.setItem("pokemon_session", JSON.stringify(session))
    }

    get(): any {
        const session = localStorage.getItem("pokemon_session")
        return session ? JSON.parse(session) : false
    }
}
